# Image Analysis Training Resources

![](https://git.embl.de/grp-bio-it/image-analysis-training-resources/badges/master/pipeline.svg)

This project is intended to collect together various resources that can be useful
when planning/delivering training in image analysis.

Detailed guidance for contributing can be found in [`CONTRIBUTING.md`](CONTRIBUTING.md).

## Repository

Current repository structure:

- `_includes/`: among other things, contains folders holding activities and exercises specific to different platforms for image analysis, for each module
- `_layouts/`, `_sass/`, `Gemfile`, `_config.yml`, `.gitlab-ci.yml`: material for building the webpages associated with this repository
- `figures/`: a collection of illustrations/diagrams used to help explain the concepts in each module.
- `image_data/`: image files used in activities and exercises
- `modules/`: all of the individual module pages are collected here
- `src/`: code for image analysis
- `workshops/`: information on workshops taught using this material
- `example_images.md`: intended to contain links to images that would make good examples when teaching
- `index.md`: content for the website landing page

## Website

Please visit the repository's [website](https://grp-bio-it.embl-community.io/image-analysis-training-resources/).
